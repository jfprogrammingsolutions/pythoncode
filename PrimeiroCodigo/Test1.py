# -*- coding: utf-8 -*-

# print('Welcome to python course')
# print("Welcome to python course")
# print("8+5*13")
# print("Python is fun", 31, 45, "Bom dia")
# print("Hello" + "Joseanne Viana")
# print ("Hello" +", "+"Joseanne")
# print ("Hello " + "Joseanne" +" You are \t spliting \t spaces \n and Lines")
name = "Joseanne"
# print(name)
# print("Hello" , name)
# greeting = "Oi "
age = 31     # essa variavel armazena a idade do usuario
# print(name)
# # print(name + ' ' + age)
# print(name, age)
# # print(name + age )
print ('%s is %d years old' % (name, age))
print ('{} is {} years old'.format(name , age))
print (f'{name} is {age} years old')

print ("Please, Enter your name: ")
student_name = input()
print("Welcome,", student_name)